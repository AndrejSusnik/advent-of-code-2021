#pragma once

#include <fstream>
#include <set>
#include <vector>
#include <string>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <ranges>

template <typename T>
std::vector<T> getInputAsVec(std::string inputFile)
{
    std::vector<T> out;
    auto file = std::ifstream(inputFile);
    if (!file.is_open())
    {
        return {};
    }

    std::string line;
    while (file.good())
    {
        file >> line;
            out.push_back(std::atoi(line.c_str()));
    }

    return out;
}

int getDepth(const std::vector<int>& values) {
    std::vector<int> tmp;
    std::transform(values.begin(), values.end(), values.begin() + 1, std::back_inserter(tmp), [](const auto& a, const auto &b) {return b > a ? 1 : 0; });

    return std::accumulate(tmp.begin(), tmp.end(), 0);
}

int getDepthRunningAverage(const std::vector<int>& values) {
    std::vector<int> tmp = std::vector<int>(values.size() / 3);

    for(auto i = values.begin(); i != values.end() - 3; i++) {
        tmp.push_back(*i + *(i+1) + *(i + 2));
    }

    return getDepth(tmp);
}