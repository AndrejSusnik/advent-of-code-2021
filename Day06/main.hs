import Data.List.Split ( splitOn );
import Text.Printf (printf)

main :: IO ()
main = do
    test_input <- readFile "test_input"
    
    printf "Test input part 1: %d\n" $ sum $ applyNtimes 80 simulateGeneration $ parseFile test_input
    printf "Test input part 2: %d\n" $ sum $ applyNtimes 256 simulateGeneration $ parseFile test_input

    input <- readFile "input"
    printf "Part 1: %d\n" $ sum $ applyNtimes 80 simulateGeneration $ parseFile input
    printf "Part 2: %d\n" $ sum $ applyNtimes 256 simulateGeneration $ parseFile input


parseFile :: [Char] -> [Int]
parseFile str = zipWith  (\x y -> countOccur' y) (replicate 9 0) [0..]
    where countOccur' x = length $ filter (== x) $ map read $ splitOn "," str

simulateGeneration :: [Int] -> [Int]
simulateGeneration gen = addBreed (head gen) (take len . drop (1 `mod` len) . cycle $ gen)
                   where len = length gen

addBreed :: Int -> [Int] -> [Int]
addBreed n = zipWith (\ i x -> (if i == 6 then x + n else x)) [0..]

applyNtimes :: (Num n, Ord n) => n -> (a -> a) -> a -> a
applyNtimes 1 f x = f x
applyNtimes n f x = f (applyNtimes (n-1) f x)