program Day3;
{$mode objfpc}{$H+}
{$ASSERTIONS ON} 
uses
  SysUtils, Classes, Strings, Math;
const
  TEST_FILE = 'test_input';
  INPUT_FILE = 'input';
type
  TPair = record
    Ones: integer;
    Zeros: integer;
  end;

  ATPair = array of TPair;
var
  TestInput, Input: TStringList;

  function GetDataFromFile(FileName: string): TStringList;
  var
    List: TStringList;
  begin
    if not FileExists(FileName) then
      Exit(nil);

    List := TStringList.Create;
    List.LoadFromFile(FileName);

    Result := List;
  end;

  function BinToDec(Arr: ATPair; Len: integer; Most: boolean): integer;
  var
    I, Res: integer;
  begin
    Res := 0;
    for I := Len - 1 downto 0 do
    begin
      if Most and (Arr[I].Ones > Arr[I].Zeros)
         or (not Most) and (Arr[I].Ones < Arr[I].Zeros) then
        Res := Res + Round(Math.Power(2, I));
    end;
    Result := Res;
  end;

  function BinToDec(Bin: string): integer;
  var
    I, Res: integer;
  begin
    Res := 0;
    for I := 1 to Bin.Length do
    begin
      if Bin[I] = '1' then
        Res := Res + Round(Math.Power(2, Bin.Length - I));
    end;
    Result := Res;
  end;

  function GetRating(const List: TStringList; Oxy: boolean): integer;
  var
    I, InpLen, CLen, Deleted: integer;
    Tmp: string;
    MCB: TPair;
    MCBC: char;
    TmpList: TStringList;
  begin
    TmpList := TStringList.Create;
    for I := 0 to List.Count - 1 do
    begin
      TmpList.Add(Copy(List[I], 0, List[I].Length));
    end;

    InpLen := StrLen(PChar(List[0]));
    for CLen := 1 to InpLen do
    begin
      MCB.Ones := 0;
      MCB.Zeros := 0;
      for I := 0 to TmpList.Count - 1 do
      begin
        Tmp := TmpList[I];
        if Tmp[CLen] = '1' then
          Inc(MCB.Ones)
        else
          Inc(MCB.Zeros);
      end;

      if (Oxy and (MCB.Ones >= MCB.Zeros))
         or ((not Oxy) and (MCB.Ones < MCB.Zeros)) then
        MCBC := '1'
      else
        MCBC := '0';

      Deleted := 0;
      for I := 0 to TmpList.Count - 1 do
      begin
        if TmpList[I - Deleted][CLen] <> MCBC then
        begin
          TmpList.Delete(I - Deleted);
          Inc(Deleted);
        end;
      end;

      if TmpList.Count = 1 then
      begin
        Result := BinToDec(TmpList[0]);
        TmpList.Clear;
      end;
    end;
  end;

  function GetFuelConsumption(List: TStringList): integer;
  var
    I, J, InpLen: integer;
    Tmp: string;
    MCB: ATPair;
  begin
    InpLen := StrLen(PChar(List[0]));
    SetLength(MCB, InpLen);
    for I := 0 to List.Count - 1 do
    begin
      Tmp := List[I];
      for J := 1 to Tmp.Length do
      begin
        if Tmp[J] = '1' then
          Inc(MCB[InpLen - J].Ones)
        else
          Inc(MCB[InpLen - J].Zeros);
      end;
    end;
    Result := BinToDec(MCB, InpLen, True) * BinToDec(MCB, InpLen, False);
  end;

begin
  TestInput := GetDataFromFile(TEST_FILE);
  WriteLn('Test result is ', GetFuelConsumption(TestInput));
  Assert(GetFuelConsumption(TestInput) = 198);
  WriteLn('Test result part 2 is ', GetRating(TestInput, True) *
                                    GetRating(TestInput, False));
  Assert(GetRating(TestInput, True) * GetRating(TestInput, False) = 230);

  Input := GetDataFromFile(INPUT_FILE);
  WriteLn('Result Part 1: ', GetFuelConsumption(Input));
  WriteLn('Result Part 2: ',  GetRating(Input, True) *
                              GetRating(Input, False));

  TestInput.Free;
  Input.Free;
end.
