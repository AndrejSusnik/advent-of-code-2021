#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#define MAX_LEN 2048

char buffer[MAX_LEN];

int parse_file_part1(FILE *file)
{
    int vpos = 0, hpos = 0;

    while (!feof(file))
    {
        fgets(buffer, MAX_LEN, file);

        char *token = strtok(buffer, " ");
        if (!token)
            break;

        const int step = atoi(strtok(NULL, " "));

        if (strcmp(token, "forward") == 0)
        {
            vpos += step;
        }
        else if (strcmp(token, "down") == 0)
        {
            hpos += step;
        }
        else if (strcmp(token, "up") == 0)
        {
            hpos -= step;
        }
    }

    return vpos * hpos;
}

int parse_file_part2(FILE *file)
{
    int vpos = 0, hpos = 0, aim = 0;

    while (!feof(file))
    {
        fgets(buffer, MAX_LEN, file);

        char *token = strtok(buffer, " ");
        if (!token)
            break;

        const int step = atoi(strtok(NULL, " "));

        if (strcmp(token, "forward") == 0)
        {
            hpos += step;
            vpos += step * aim;
        }
        else if (strcmp(token, "down") == 0)
        {
            aim += step;
        }
        else if (strcmp(token, "up") == 0)
        {
            aim -= step;
        }
    }

    return vpos * hpos;
}

int main(void)
{

    FILE *test_input = fopen("test_input", "r");
    if (!test_input)
    {
        printf("Unable to open file\n");
        return 1;
    }

    const int res = parse_file_part1(test_input);
    assert(res == 150);
    printf("Final pos is %d\n", res);

    fclose(test_input);

    test_input = fopen("input", "r");
    if (!test_input)
    {
        printf("Unable to open file\n");
        return 1;
    }

    const int part1 = parse_file_part1(test_input);
    printf("Part 1: %d\n", part1);

    fclose(test_input);

    test_input = fopen("test_input", "r");
    if (!test_input)
    {
        printf("Unable to open file\n");
        return 1;
    }

    const int test_part2 = parse_file_part2(test_input);
    assert(test_part2 == 900);
    printf("Final pos is %d\n", test_part2);

    fclose(test_input);

    test_input = fopen("input", "r");
    if (!test_input)
    {
        printf("Unable to open file\n");
        return 1;
    }

    const int part2 = parse_file_part2(test_input);
    printf("Part 2: %d\n", part2);

    fclose(test_input);


    return 0;
}
