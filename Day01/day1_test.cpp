#include <gtest/gtest.h>
#include "day1.hpp"
#include <algorithm>

TEST(Day1Tests, ExampleTests)
{
    const auto vec = getInputAsVec<int>("test_input");
    const auto depth = getDepth(vec);
    ASSERT_EQ(7, depth);

    const auto firstPart = getInputAsVec<int>("input");
    const auto depthPartOne = getDepth(firstPart);
    std::cout << "Part one: " << depthPartOne << "\n";

    const auto depthPartTwo = getDepthRunningAverage(firstPart);
    std::cout << "Part two: " << depthPartTwo << "\n";
}