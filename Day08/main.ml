let test_input_file = "test_input"
let input_file = "input"

module M = Map.Make (Int)
module MS = Map.Make (String)

let read_whole_file filename =
  let ch = open_in filename in
  let s = really_input_string ch (in_channel_length ch) in
  close_in ch;
  s

let parse_file_part1 input_file_name = 
  read_whole_file input_file_name 
    |> String.split_on_char '\n'
    |> List.map (String.split_on_char '|')
    |> List.flatten |> List.map String.trim
    |> List.map (String.split_on_char ' ')
    |> List.filter (fun x -> List.length x = 4)
    |> List.flatten |> List.map (fun x -> String.length x)
    |> List.filter (fun x -> x = 7 || x = 2 || x = 3 || x = 4)
    |> List.length

let decode_num x = 
  match String.length x with
    2 -> 1
   |3 -> 7
   |4 -> 4
   |7 -> 8
   |_ -> -1

let decode_num_dict str decode_map decode_map_r =
  let contains_1 = M.find 1 decode_map
    |> String.for_all (fun x -> String.contains str x) in
  let v4 = M.find 4 decode_map in
  let contains_4 = v4
    |> String.for_all (fun x -> String.contains str x) in
  let diff_4 = str
    |> String.to_seq |> List.of_seq
    |> List.filter (fun x ->  not (String.contains v4 x))
    |> List.length in
   
  match MS.find str decode_map_r with
    1 -> 1
   |4 -> 4
   |7 -> 7
   |8 -> 8
   |_ -> (
      if String.length str = 6 && contains_4 then
        9
      else if String.length str = 6 && contains_1 && not contains_4 then
        0
      else if String.length str = 6 && not contains_1 then
        6
      else if String.length str = 5 && contains_1 then 
        3
      else if String.length str = 5 && diff_4 = 2 then
        5
      else 
        2
    )

let sortKey s = 
  String.to_seq s |> List.of_seq |> List.sort Char.compare |> List.to_seq |> String.of_seq;;

let decode_line decode_map decode_rev_map dinfo res =
  let final_map = dinfo
    |> List.map (fun x ->  x, decode_num_dict x decode_map decode_rev_map)
    |> List.to_seq |> MS.of_seq in

  res 
  |> List.map (fun x -> MS.find x final_map) 
  |> List.map (fun x -> Char.chr (x + 48))
  |> List.to_seq |> String.of_seq|> int_of_string

let parse_file_part2 input_file_name = 
  let el = read_whole_file input_file_name
    |> String.split_on_char '\n'
    |> List.map (String.split_on_char '|')
    |> List.flatten |> List.map String.trim
    |> List.map (String.split_on_char ' ')
    |> List.map (fun x -> List.map (sortKey) x) in
  
  let dlist = el
    |> List.filter (fun x -> List.length x != 4) in

  let decode_map = dlist
    |> List.map (fun x -> List.map (fun x -> decode_num x, x) x)
    |> List.map (fun x -> M.of_seq (List.to_seq x)) in

  let decode_rev_map = dlist
    |> List.map (fun x -> List.map (fun x -> x, decode_num x) x)
    |> List.map (fun x -> MS.of_seq (List.to_seq x)) in

  el
    |> List.filter (fun x -> List.length x = 4)
    |> List.mapi (fun i x -> decode_line (List.nth decode_map i) (List.nth decode_rev_map i) (List.nth dlist i) x)
    |> List.fold_left (+) 0
 
let () = 
  assert ((parse_file_part1 test_input_file) = 26);
  print_string "Part 1:"; print_int (parse_file_part1 input_file) ; print_string "\n";
  assert ((parse_file_part2 test_input_file) = 61229);
  print_string "Part 2:"; print_int (parse_file_part2 input_file) ; print_string "\n";