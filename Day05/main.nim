import strutils
import std/sequtils
import std/sugar

proc readFile(fileName: string, part1: bool):seq[seq[seq[int]]] = 
    let file = open(fileName)

    defer: file.close()
    let points = collect(newSeq):
        for line in file.lines:
            line.split("->").map(x => x.strip().split(',').map(x => parseInt(x)))
    if part1:
        points.filter(x => (x[0][0] == x[1][0]) or (x[0][1] == x[1][1]))
    else:
        points.filter(x => (x[0][0] == x[1][0]) or (x[0][1] == x[1][1]) or (abs(x[0][0] - x[1][0]) == abs(x[0][1] - x[1][1])))
        

proc expand(points: seq[seq[int]]): seq[seq[int]] =
    if points[0][0] == points[1][0] or points[0][1] == points[1][1]:
        collect(newSeq):
            for i in min(points[0][0], points[1][0])..max(points[1][0], points[0][0]):
                for j in min(points[0][1],points[1][1])..max(points[1][1], points[0][1]):
                    toSeq([i, j])
    else:
        collect(newSeq):
            for i in 0..abs(points[0][0] - points[1][0]):
                var x = points[0][0]
                var y = points[0][1]
                if points[0][0] < points[1][0]:
                    x += i
                else:
                    x -= i                
                if points[0][1] < points[1][1]:
                    y += i
                else:
                    y -= i
                toSeq([x, y])

    

proc checkVents(ventLines: seq[seq[seq[int]]]): int = 
    let maxX = ventLines.map(x => x[0]).map(x => max(x[0], x[1])).max()
    let maxY = ventLines.map(x => x[1]).map(x => max(x[0], x[1])).max()
    
    let extendedLines = ventLines.map(expand)
    var bottom = toSeq(0..maxY).map(x => toSeq(0..maxX).map(x => 0))
    for line in extendedLines:
        for point in line:
            bottom[point[0]][point[1]] += 1

    bottom.map(x => x.countIt(it >= 2)).foldl(a + b)



let test_vents = readFile("test_input", true)
doAssert checkVents(test_vents) == 5

let input_vents = readFile("input", true)
echo  "Part 1: ", checkVents(input_vents)

let test_vents_2 = readFile("test_input", false)
doAssert checkVents(test_vents_2) == 12

let input_vents_2 = readFile("input", false)
echo "Part 2: ", checkVents(input_vents_2)