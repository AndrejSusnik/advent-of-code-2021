from collections import defaultdict

edges = defaultdict(list)
for fr, to in (v.strip().split("-") for v in open("input")):
    if to != "start":
        edges[fr].append(to)
    if fr != "start":
        edges[to].append(fr)

for part in range(2):
    count = 0
    check = [("start", ["start"], False)]
    for node, path, twice in check:
        for next in edges[node]:
            if next == "end":
                count += 1
            elif next not in path or (part and not twice):
                check.append((next, path + [next] * next.islower(), twice or next in path))

    print(f"Part {part}: {count}")