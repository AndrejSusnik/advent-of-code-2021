# Advent of code 2021

The challenge of this advent of code will be to solve problems with as many programming languages as possible.

## Day 1
* Language: C++

## Day 2
* Language: C

## Day 3
* Language: Object Pascal

# Day 4
* Language: D

# Day 5
* Language: Nim

# Day 6
* Language: Haskell

# Day 7
* Language: Scala

# Day 8
* Language: Ocaml

# Day 9
* Language: Crystal

# Day 10
* Language: V

# Day 11
* Language: Python

# Day 12
* Language: Python

Remaining languages:
Javascript,
TypeScript,
Java,
Rust,
C#, 
Go,
PHP,
Dart,
Kotlin,
Lua,
Julia,
Ruby,
Groovy, Visual basic, Fortran, R, Matlab, Purescript, Elixir, Zig