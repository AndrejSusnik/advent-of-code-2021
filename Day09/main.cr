require "spec"

def eval(x : Int32, i : Int32, mat : Array(Int32), xDim : Int32)
    is_low = i % xDim - 1 >= 0 ? x < mat[i - 1] : true
    is_low &= i % xDim + 1 < xDim ? x < mat[i + 1] : true
    is_low &= i - xDim >= 0 ? x < mat[i - xDim] : true
    is_low &= i + xDim < mat.size ? x < mat[i + xDim] : true
    
    is_low ? x + 1 : 0
end

def solve(fileName : String)
    content = File.open(fileName) do |file|
        file.gets_to_end
    end

    input_mat = content.split("\n").map{ |x| x.chars.map{|y| y.to_i()}}
    xDim = input_mat[0].size
    flat_mat = input_mat.flatten
    input_mat.flatten.map_with_index{|x, y| eval(x, y, flat_mat, xDim)}.sum
end

def getBasinSize(i, xDim, yDim, checked_indicies)
    if checked_indicies.includes?(i)
        0
    else
        checked_indicies.add(i)
        ret = i % xDim - 1 >= 0 ? getBasinSize(i - 1, xDim, yDim, checked_indicies) : 0
        ret += i % xDim + 1 < xDim ? getBasinSize(i + 1, xDim, yDim, checked_indicies) : 0
        new_y = i - xDim
        ret += new_y >= 0 ? getBasinSize(new_y, xDim, yDim, checked_indicies) : 0
        new_y = i + xDim
        ret += new_y < yDim ? getBasinSize(new_y, xDim, yDim, checked_indicies) : 0
        ret + 1
    end
end

def solve2(fileName : String)
    content = File.open(fileName) do |file|
        file.gets_to_end
    end

    input_mat = content.split("\n").map{ |x| x.chars.map{|y| y.to_i()}}
    xDim = input_mat[0].size
    input_mat = input_mat.flatten
    
    checked_indicies = Set.new(input_mat.map_with_index{|x, y| {x, y} }.reject do |x|
        x[0] != 9
    end.map{|x| x[1]})
    basinSizes = Array(Int32).new
    i = 0
    while i < input_mat.size 
        if !checked_indicies.includes?(i)
            basinSizes.push(getBasinSize(i, xDim, input_mat.size, checked_indicies))
        end
        i = i + 1
    end
    sorted = basinSizes.sort {|x, y| y <=> x}
    sorted[0] * sorted[1] * sorted[2]
end

((solve "test_input") == 15).should be_true
printf "Part 1: %d\n", (solve "input")
((solve2 "test_input") == 1134).should be_true
printf "Part 2: %d\n", (solve2 "input")