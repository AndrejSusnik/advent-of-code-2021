import std.stdio;
import std.file;
import std.array;
import std.string;
import std.conv;
import std.algorithm;

class BingSheet
{
    this()
    {
        sheet = new int[][](5, 5);
    }

    int checkWin()
    {
        bool[5] row = true, column = true;
        int res = 0;
        foreach (int i; 0 .. 5)
        {
            foreach (int j; 0 .. 5)
            {
                const bool val = checked[i][j];
                if (!val)
                    res += sheet[i][j];

                row[i] = row[i] && val;
                column[j] = column[j] && val;

            }
        }

        if (any(row[]) || any(column[]))
            return res;

        return -1;
    }

    void mark(int num)
    {
        foreach (int i; 0 .. 5)
        {
            uint pos = cast(uint) sheet[i].countUntil!(c => c == num);
            if (pos < 5)
                checked[i][pos] = true;
        }
    }

    int[][] sheet;
    bool[5][5] checked;

}

int parse_file(string file_name, bool part1)
{
    File input_file = File(file_name, "r");

    const auto game_numbers = to!(int[])(split(strip(input_file.readln()), ","));
    int index = -1, line_index = 0;
    BingSheet[] sheets;

    while (!input_file.eof())
    {
        auto line = to!(int[])(split(chomp(input_file.readln())));
        if (!line.length)
        {
            index++;
            sheets.length = index + 1;
            sheets[sheets.length - 1] = new BingSheet;
            line_index = 0;
        }
        else
        {
            sheets[index].sheet[line_index] = line;
            line_index++;
        }
    }

    input_file.close();

    foreach (int num; game_numbers)
    {
        if (part1)
        {
            foreach (BingSheet key; sheets)
            {
                key.mark(num);
                int res = key.checkWin();
                if (res != -1)
                {
                    return res * num;
                }
            }
        }
        else
        {
            int deleted = 0;
            const int len = cast(int) sheets.length;
            foreach (int i; 0 .. len)
            {
                const int idx = i - deleted;
                sheets[idx].mark(num);
                const int res = sheets[idx].checkWin();
                if (res != -1)
                {
                    if (sheets.length == 1)
                        return res * num;
                    else
                        sheets = remove(sheets, i - deleted++);

                }
            }

        }
    }

    return 0;
}

void main()
{
    const string test_input = "test_input";
    const string input = "input";

    int res = parse_file(test_input, true);
    assert(res == 4512);
    writeln("Test result for part 1: ", res);

    int part1 = parse_file(input, true);
    writeln("Result for part 1: ", part1);

    int res2 = parse_file(test_input, false);
    assert(res2 == 1924);
    writeln("Test result for part 2: ", res2);

    int part2 = parse_file(input, false);
    writeln("Result for part 2: ", part2);

}
