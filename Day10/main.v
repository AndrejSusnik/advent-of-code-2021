import os

fn eval(closings []rune) u64 {
    v2 := { `(`: 1, `[`: 2, `{`: 3, `<`: 4}
    mut score := u64(0)
    for c in closings.reverse() { score = score * 5 + u64(v2[c]) }
    return score
}

fn solve(file_name string, part1 bool) u64 {
    closings := {`(`:`)`, `{`: `}`, `[`:`]`, `<`:`>`}
    values1 := {`)`: 3, `]`: 57, `}`: 1197, `>`: 25137 }
    closing_brackets := '>)]}'
    lines := os.read_lines(file_name) or { return 0 }
    mut scores := []u64{cap: lines.len}
    mut sum := 0
    
    for line in lines {
        mut l := []rune{cap: line.len / 2}
        mut discard := false
        for c in line.runes() {
            if c in closing_brackets.runes() {
                if closings[l.pop()] != c {
                    sum += values1[c]
                    discard = true
                    break
                }
            }
            else { l << c }
        }
        if !discard { scores << eval(l) }  
    }

    if part1 { return u64(sum) }
    scores.sort()
    return scores[scores.len / 2]
}

fn main() {
    test_input_file := 'test_input'
    input_file := 'input'
    
    assert solve(test_input_file, true) == 26397
    println("Part 1: ${solve(input_file, true)}")
    assert solve(test_input_file, false) == 288957
    println("Part 2: ${solve(input_file, false)}")
}