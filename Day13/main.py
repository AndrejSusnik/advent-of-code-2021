def solve(input_file):
    folded_data = {tuple(int(x) for x in line.strip().split(",")) for line in open(input_file) if "fold" not in line and line != '\n'}
    folds = [line.strip().split(" ")[2].split("=") for line in open(input_file) if "fold" in line]
    for fold in folds:
        direction, around = fold
        around = int(around)
        if direction == 'y':
            folded_data = {point if point[1] < around else (point[0], around - (point[1] - around)) for point in folded_data }
        else:
            folded_data = {point if point[0] < around else (around - (point[0] - around), point[1]) for point in folded_data }

    print(len(folded_data))
    return folded_data

def visualize(data):
    grid = [["." for _ in range(50)] for _ in range(7)]
    for x, y in data:
        grid[y][x]="#"

    for l in grid:
        print("".join(l))

solve("test_input")
visualize(solve("input"))