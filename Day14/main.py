from collections import defaultdict
from math import ceil

def solve(file_name, part):
    initial = [char for line in open(file_name) if "->" not in line and line != "\n" for char in line.strip()]
    rules = {line.strip().split(" -> ")[0]:line.strip().split(" -> ")[1] for line in open(file_name) if "->" in line}
    
    counts = defaultdict(int)
    for a,b in zip(initial, initial[1:]):
        counts[a+b] += 1

    for _ in range(40 if not part else 10):
        new_cnts = defaultdict(int)
        for a,b in counts.keys():
            new_cnts[a + rules[a+b]] += counts[a + b]
            new_cnts[rules[a+b] + b] += counts[a + b]
            
        counts = new_cnts
    
    individual = defaultdict(int)
    for a,b in counts.keys():
        individual[a] += counts[a+b]
        individual[b] += counts[a+b]

    return ceil(max(individual.values()) / 2) - ceil(min(individual.values()) / 2)


assert(solve("test_input", True) == 1588)
print(f"Part 1: {solve('input', True)}")


assert(solve("test_input", False) == 2188189693529)
print(f"Part 2: {solve('input', False)}")