
def solve(file_name):
    data = [list(map(lambda x: int(x), line.strip())) for line in open(file_name)]
    accum = 0
    for _ in range(100):
        data, lumen = generation(data)
        accum += lumen
    return accum

def solve2(file_name):
    data = [list(map(lambda x: int(x), line.strip())) for line in open(file_name)]
    cnt = 0
    while True:
        data, lumen = generation(data)
        if lumen == len(data) * len(data[0]):
            return cnt + 1
        cnt += 1
    
    

def get_flashed(o_list, coords, flashed):
    if coords in flashed:
        return 0
    else:
        flashed.add(coords)
        x, y = coords
        sum = 1
        for i in range(-1, 2):
            for j in range(-1, 2):
                if x + i >= 0 and x + i < len(o_list) and y + j >= 0 and y + j < len(o_list[0]):
                    o_list[x + i][y + j] += 1
                    if o_list[x + i][y + j] > 9:
                        sum += get_flashed(o_list, (x + i, y + j), flashed)
                else:
                    sum += 0
        return sum

    

def generation(o_list):
    o_list = list(map(lambda x: list(map(lambda y: y + 1, x)), o_list))
    flashed_s = 0
    flashed = set()

    for i in range(len(o_list)):
        for j in range(len(o_list[0])):
            if (i,j) in flashed:
                continue
            else:
                if o_list[i][j] > 9:
                    flashed_s += get_flashed(o_list, (i, j), flashed)

    o_list = list(map(lambda x: list(map(lambda y: 0 if y > 9 else y, x)), o_list)) 
    return o_list, flashed_s

print(f"Test input1: {solve('test_input')}")
print(f"Part 1: {solve('input')}")
print(f"Test input2: {solve2('test_input')}")
print(f"Part 2: {solve2('input')}")